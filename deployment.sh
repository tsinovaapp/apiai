sudo docker stop $(sudo docker ps -q --filter ancestor=leodevelb/api-artificial-intelligence:1.0.0)
sudo docker rm $(sudo docker ps -a -q --filter ancestor=leodevelb/api-artificial-intelligence:1.0.0)
sudo docker rmi $(sudo docker images --format '{{.Repository}}:{{.Tag}}' | grep 'leodevelb/api-artificial-intelligence:1.0.0')

sudo docker build -t leodevelb/api-artificial-intelligence:1.0.0 .
sudo docker run -d -p 5555:5000 leodevelb/api-artificial-intelligence:1.0.0
sudo docker ps
