import pandas as pd
from fbprophet import Prophet
from pandas.plotting import register_matplotlib_converters

register_matplotlib_converters()
df = pd.read_csv('data.csv')

m = Prophet(changepoint_prior_scale=0.01)
m.fit(df)

future = m.make_future_dataframe(periods=24*7, freq="H", include_history=False)
future.tail()

forecast = m.predict(future)
#print(forecast[['ds', 'yhat', 'yhat_lower', 'yhat_upper']].tail())

forecast[['ds', 'yhat', 'yhat_lower', 'yhat_upper']].to_csv('output.csv')

fig1 = m.plot(forecast, xlabel='Período', ylabel='Consumo de armazenamento (Gb)')
fig1.savefig('previsao.png', formrat='png', dpi=1200);
