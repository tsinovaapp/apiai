from elasticsearch import Elasticsearch
from luminol.modules.time_series import TimeSeries

class DataList(object):

    # rotina para buscar informações no elasticsearch
    @staticmethod
    def search(host, port, index, body, field_timestamp_name, field_value_name):
        series = TimeSeries([])
        size = 1000

        es = Elasticsearch([{"host": host, "port": port}])
        data = es.search(index=index, scroll="2m", size=size, body=body)

        sid = data['_scroll_id']
        scroll_size = len(data['hits']['hits'])

        for item in data['hits']['hits']:
            dateobj = item["_source"][field_timestamp_name]
            key = ""
            try:
                key = datetime.strptime(dateobj, '%Y-%m-%dT%H:%M:%S.%fZ').timestamp()
            except:
                key = datetime.strptime(dateobj, '%Y-%m-%dT%H:%M:%SZ').timestamp()
            series.__setitem__(key, item["_source"][field_value_name])

        while scroll_size > 0:
            data = es.scroll(scroll_id=sid, scroll='2m')
            for item in data['hits']['hits']:
                dateobj = item["_source"][field_timestamp_name]
                key = ""
                try:
                    key = datetime.strptime(dateobj, '%Y-%m-%dT%H:%M:%S.%fZ').timestamp()
                except:
                    key = datetime.strptime(dateobj, '%Y-%m-%dT%H:%M:%SZ').timestamp()
                series.__setitem__(key, item["_source"][field_value_name])
            sid = data['_scroll_id']
            scroll_size = len(data['hits']['hits'])

        return series
