class Util(object):

    # converte o array de valores do tipo string em tipo float
    @staticmethod
    def loadArrayStringToFloat(array):
        newArray = []
        for value in array:
            if isinstance(value, str):
                newArray.append(float(value.strip()))
            else:
                newArray.append(float(value))
        return newArray
