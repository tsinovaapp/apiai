from luminol.anomaly_detector import AnomalyDetector
from luminol.modules.time_series import TimeSeries

score_anomaly = 1.5

series = TimeSeries({0: 0, 1: 0.5, 2: 1, 3: 1, 4: 1, 5: 0, 6: 0, 7: 0, 8: 0, 9: 40, 10: 40, 11: 45, 13: 2, 12: 1})

my_detector = AnomalyDetector(series)

score = my_detector.get_all_scores()
anomalies = []

for timestamp, value in score.iteritems():
    if value >= score_anomaly:
       anomalies.append([timestamp, value])

print(anomalies)
