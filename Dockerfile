# imagem python
FROM leodevelb/apiai-image-os

# responsável
MAINTAINER Leonardo Barbosa <leobar1995@gmail.com>

# instala a apiai
RUN cd /usr/share/ && mkdir apiai
ADD . /usr/share/apiai/

# diretório inicial de trabalho
WORKDIR /usr/share/apiai

# expoẽ a porta para uso
EXPOSE 5000

# executa a apiai
ENTRYPOINT ["python"]
CMD ["apiai.py"]
