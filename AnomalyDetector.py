import numpy as np
import pandas as pd
import json

class AnomalyDetector(object):

    def __init__(self, data=[], threshold=3.5):
        self.data = data
        self.threshold = threshold

    def detectOutliers(self, dataTest):
        median_y = np.median(dataTest)
        median_absolute_deviation_y = np.median([np.abs(y - median_y) for y in dataTest])
        modified_z_scores = [0.6745 * (y - median_y) / median_absolute_deviation_y
                             for y in dataTest]
        outliers = np.where(np.abs(modified_z_scores) > self.threshold)
        return json.loads(pd.Series(outliers).to_json(orient='values'))[0]

    def isAnomaly(self, value):
        dataTest = self.data.copy()
        dataTest.append(value)
        i = len(dataTest) - 1
        outliers = self.detectOutliers(dataTest)
        for pos in outliers:
            if pos == i:
                return 1
        return 0
