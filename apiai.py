from flask import (Flask, request, jsonify)
import json
from datetime import datetime
from AnomalyDetector import AnomalyDetector
from Util import Util

app = Flask(__name__)

# endpoint para realizar uma previsão futura
@app.route('/api/ai/prediction', methods=["GET"])
def prediction():
    return "endpoint: /api/ai/prediction"

# endpoint para detectar anomalias
@app.route('/api/ai/is_anomaly', methods=["GET", "POST"])
def anomaly():

    data = json.loads(request.data)
    dataTraining = Util.loadArrayStringToFloat(data['data_training'].split(","))
    value = float(data['value_check'])

    anomalyDetector = AnomalyDetector(dataTraining)
    anomaly = anomalyDetector.isAnomaly(value)

    return jsonify(
        anomaly=anomaly
    )

if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0')
